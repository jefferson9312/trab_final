<?php
include './lib/sanitize.php';

function getPost($id){//só usa no edit_post
    require 'db_credentials.php';

    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn) {
    die("Problemas ao conectar com o BD!<br>".
        mysqli_connect_error());
    }

    $id = sanitize($id);
    $id = mysqli_real_escape_string($conn, $id);

    $sql = "SELECT id, titulo, texto, img_path FROM post WHERE id=".$id;

    if(!($resultado = mysqli_query($conn,$sql))){
        die("Problemas para carregar Post do BD!<br>".mysqli_error($conn));
    }

    mysqli_close($conn);

    if (mysqli_num_rows($resultado) != 1) {
        die("Id de Post incorreto.");
    }else{
        return $resultado;
    }

}

function getAllPosts(){
    require 'db_credentials.php';
    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn) {
        die("Problemas ao conectar com o BD!<br>".
             mysqli_connect_error());
    }

    $sql = "SELECT * FROM post ORDER BY id DESC";

    if($resultado = mysqli_query($conn, $sql)){
        return $resultado;  
    }else{
        die("Problemas para carregar Posts do BD!<br>". mysqli_error($conn));
    }

    mysqli_close($conn);
}

function insertPost($titulo, $texto){
    require 'db_credentials.php';
    require 'login_authenticate.php';

    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn) {
        die("Problemas ao conectar com o BD!<br>".
             mysqli_connect_error());
    }

    $titulo = sanitize($titulo);
    $texto = sanitize($texto);  
    $titulo = mysqli_real_escape_string($conn, $titulo);
    $texto = mysqli_real_escape_string($conn, $texto);

    $data_criado = date("Y-m-d");

    $img_path = uploadImg();

    $sql = "INSERT INTO post (idUser, titulo, texto, img_path, data_post)
    VALUES ('$user_id','$titulo', '$texto', '$img_path', '$data_criado')";

 
    if(!mysqli_query($conn,$sql)){
        die("Problemas para executar ação no BD!<br>". mysqli_error($conn));
    }
    
    mysqli_close($conn);
}

function deletePost($id){
    require 'db_credentials.php';
    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn){
        die("Problemas ao conectar com o BD!<br>".
             mysqli_connect_error());
    }

    $id = sanitize($id);  
    $id = mysqli_real_escape_string($conn, $id);

    $sql = "DELETE FROM post WHERE id=" . $id;
    deleteImg($id);
    
    if(!mysqli_query($conn,$sql)){
        die("Problemas para executar delete no BD!<br>". mysqli_error($conn));
    }
    

    mysqli_close($conn);
}

function updatePost($id, $titulo, $texto){
    require 'db_credentials.php';
    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn){
        die("Problemas ao conectar com o BD!<br>".
             mysqli_connect_error());
    }
    $titulo = sanitize($titulo);
    $texto = sanitize($texto);  
    $id = sanitize($id);  
    $titulo = mysqli_real_escape_string($conn, $titulo);
    $texto = mysqli_real_escape_string($conn, $texto);
    $id = mysqli_real_escape_string($conn, $id);

    $data_criado = date("Y-m-d");

    deleteImg($id);
    $img_path = uploadImg();
    
    $sql = "UPDATE post SET titulo='$titulo', texto='$texto', img_path='$img_path',
    data_post='$data_criado' WHERE id=" . $id;
    
    
    if(!mysqli_query($conn,$sql)){
        die("Problemas para executar ação no BD!<br>". mysqli_error($conn));
    }
    

    mysqli_close($conn);
}

function uploadImg(){
    $arq_img = pathinfo($_FILES["image"]["name"]); //foto.png
    $img_path = "./img_uploads/". uniqid("") . ".". $arq_img["extension"];
    move_uploaded_file($_FILES["image"]["tmp_name"], "$img_path"); //envia pra img_uploads
    return $img_path;
}

function deleteImg($id){
    require 'db_credentials.php';
    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn){
        die("Problemas ao conectar com o BD!<br>".
             mysqli_connect_error());
    }

    $id = sanitize($id);  
    $id = mysqli_real_escape_string($conn, $id);

    $sql = "SELECT img_path FROM post where id=".$id;
    
    if($resultado = mysqli_query($conn, $sql)){
        $row = mysqli_fetch_assoc($resultado);
        $img_path = $row["img_path"];
        if (file_exists($img_path)){
            unlink($img_path);  //deleta imagem
        }         
    }else{
        die("Problema para carregar Posts do BD!<br>". mysqli_error($conn));
    }

    mysqli_close($conn);
}

?>
