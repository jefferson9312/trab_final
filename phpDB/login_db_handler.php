<?php
include './lib/sanitize.php';

function getUser($email, $senha){
    require 'db_credentials.php';
    require "login_authenticate.php";

    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if (!$conn) {
    die("Problemas ao conectar com o BD!<br>".
        mysqli_connect_error());
    }

    $msg = "";

    $email = sanitize($email);
    $email = mysqli_real_escape_string($conn, $email);

    $senha = sanitize($senha);
    $senha = mysqli_real_escape_string($conn, $senha);


    $sql = "SELECT idUser, email, senha FROM users WHERE email='$email'";
    $resultado = mysqli_query($conn,$sql);
    if($resultado){
        if (mysqli_num_rows($resultado) > 0) {
            $user = mysqli_fetch_assoc($resultado);

            if ($user["senha"] == $senha) {
                $_SESSION["user_id"] = $user["idUser"];
                mysqli_close($conn); // Fecha conexão Antes de Sair
                header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/adm_page.php");
                exit(); //Termina Execução do Script
            }else{
                $msg = "Senha Incorreta";
            }
            
        }else{
            $msg = "Email Não Existente";
        }
        mysqli_close($conn);
        return $msg;

    }else{      
        die("Problemas para carregar usuário do BD!<br>".mysqli_error($conn));
    }
 //Só retorna se não saiu no exit();
}
?>
