<?php
require 'db_credentials.php';

// Create connection
$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Create database
$sql = "CREATE DATABASE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database created successfully<br>";
} else {
    echo "<br>Error creating database: " . mysqli_error($conn);
}

// Choose database
$sql = "USE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database changed";
} else {
    echo "<br>Error creating database: " . mysqli_error($conn);
}

// cria tabela post
$sql = "CREATE TABLE post (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    idUser INT(6) NOT NULL,
    titulo VARCHAR(100) NOT NULL,
    texto VARCHAR(1500) NOT NULL,
    img_path VARCHAR(200) NOT NULL,
    data_post DATE NOT NULL,
    FOREIGN KEY (idUser) REFERENCES users(idUser)
  )";

if (mysqli_query($conn, $sql)) {
    echo "<br>Table post created successfully";
} else {
    echo "<br>Error creating Table post: " . mysqli_error($conn);
}

//cria tabela users
$sql = "CREATE TABLE users (
    idUser INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(100) NOT NULL,
    senha VARCHAR(8) NOT NULL,
    UNIQUE (email)
  )";

if (mysqli_query($conn, $sql)) {
    echo "<br>Table users created successfully";
} else {
    echo "<br>Error creating Table users: " . mysqli_error($conn);
}

//insere 1 usuario
$sql = "insert into users (email, senha) values ('teste@teste.com', '1234');";

if (mysqli_query($conn, $sql)) {
    echo "<br>User teste@teste.com with password 1234 created successfully";
} else {
    echo "<br>Error creating Table users: " . mysqli_error($conn);
}

mysqli_close($conn);

?>