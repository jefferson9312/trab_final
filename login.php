<?php
require "./phpDB/login_db_handler.php";
require "./phpDB/login_authenticate.php";

$senha = $email = $erro_msg = "";

//Se já está logado vai pra adm_pg
if (isset($_SESSION["user_id"])) {
  header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/adm_page.php");
  exit(); //Termina Execução do Script
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (!empty($_POST["email"])) {
    $erro_msg = getUser($_POST["email"], $_POST["senha"]);
  }
  else{
    $erro_msg = "Preencha todos os campos!";
  }
}

?>
<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="./css/login.css"/>
    <link rel="icon" type="image/png" href="./images/logo.png">
		<title>Login - Manual do Viajante</title>
    <meta charset="utf-8" />
	</head>
 <body>
    
	<div id="container">
      <div id="content">
        <div id="login">
            <h1>Login</h1>
            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <input autocomplete="on" class="campos" type="email" id="email" name="email" placeholder="Login"><br>
                <input class="campos" type="password" id="senha" name="senha" placeholder="Senha"><br>
                <div id="erro-login">
                  <?php if (!empty($erro_msg)) : ?>
                    <span><?php echo $erro_msg ?></span>
                  <?php endif; ?>
                </div>
                <button>
                  <input id="botao_login" type="submit" value="Login">
                </button>
                
            </form>
        </div> 
      </div>
    </div>
 </body>
</html>