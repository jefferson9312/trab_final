$(function(){
  $("#form-test").on("submit",function(){
    titulo_input = $("input[name='titulo']");
    texto_input = $("input[name='texto']");
    img_input = $("input[name='image']");

    if(titulo_input.val() == "" || titulo_input.val() == null)
    {
      $("#erro-titulo").html("O Titulo e obrigatorio");
      return(false);
    }

    if(texto_input.val() == "" || texto_input.val() == null)
    {
      $("#erro-texto").html("O Texto da Postagem e obrigatorio");
      return(false);
    }

    if(img_input.val() == null)
    {
      $("#erro-img").html("Imagem e obrigatoria");
      return(false);
    }

    return(true);
  });
});
