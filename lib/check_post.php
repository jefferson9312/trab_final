<?php
function verifica_campo($texto){
  $texto = trim($texto);
  $texto = stripslashes($texto);
  $texto = htmlspecialchars($texto);
  return $texto;
}

$titulo = "";
$texto = "";
$data = "";
$erro = false;
$formatos_img_array = array("png", "jpg", "jpeg", "PNG", "JPG", "JPEG");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  //titulo
  if(empty($_POST["titulo"])){
    $erro_titulo = "Título é obrigatório.";
    $erro = true;
  }else{
    $titulo = verifica_campo($_POST["titulo"]);
  }

  //texto
  if(empty($_POST["texto"])){
    $erro_texto = "Texto é obrigatório.";
    $erro = true;
  }else{
    $texto = verifica_campo($_POST["texto"]);
  }

  //IMG
  if($_FILES["image"]["tmp_name"] == null){
    $erro_img = "Imagem é obrigatória.";
    $erro = true;
  }else{
    if($_FILES["image"]["size"] > 2000000){ //Por padrão PHP o max é 2mb
      $erro_img = "O Tamanho máximo da imagem é de 2MB.";
      $erro = true;
    }else{
      $arq_img = pathinfo($_FILES["image"]["name"]);
      $img_extensao = $arq_img["extension"];
      $img_name = $_FILES["image"]["name"];
      //Verifica se existe em array
      if(in_array($img_extensao, $formatos_img_array)){
        
      }else{
        $erro_img = "Formato de Arquivo Não Suportado";
        $erro = true;
      } 
    }
  }
}
?>
