<?php
include("./phpDB/post_db_handler.php");

if ($_SERVER["REQUEST_METHOD"] == "GET"){
  if (isset($_GET["id"])) {
    $resultado = getPost($_GET["id"]);
  }
}

?>
<!DOCTYPE html>
<html>

<head>
    <link type="text/css" rel="stylesheet" href="css/edit_post.css" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="icon" type="image/png" href="./images/logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="./js/check_edit_post.js"></script>


    <title>Manual do Jovem Viajante</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div id="container">
        <div id="content">
            <div id="centro-area-postagem">
                <div id="area-postagem">
                    <h1>Editar Postagem</h1>
                    <!--inputs-->
                    <form enctype="multipart/form-data" id="form-test" class="form-horizontal" method="POST" action="adm_page.php">
                        <?php $post = mysqli_fetch_assoc($resultado); ?>
                        <input type="hidden" name="id" value="<?php echo $post["id"]?>">
                        <!-- Nome -->
                        <div class="form-group <?php if (!empty($erro_titulo)) {echo "has-error";} ?>">
                            <div>
                                <input required type="text" class="form-control" autocomplete="off" name="novotitulo" maxlength="100" placeholder="Título" value="<?php echo $post["titulo"] ?>">
                            </div>
                            <div id="erro-titulo">
                            </div>
                        </div>

                        <!-- Area de Texto-->
                        <div class="form-group <?php if (!empty($erro_texto)) {echo "has-error";} ?>">
                            <textarea name="novotexto" form="form-test" placeholder="Texto da Postagem..." maxlength="1500" required><?php echo $post["texto"] ?></textarea>
                            <div id="erro-texto">
                            </div>
                        </div>

                        <!-- IMG -->
                        <div class="form-group <?php if (!empty($erro_img)) {echo "has-error";} ?>">
                            <label for="inputImg">Imagem</label>
                            <div>
                                <input required type="file" class="form-control" name="image">
                            </div>
                            <div id="erro-img">
                            </div>
                        </div>
                        <!-- Botão de Submissão -->
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-success">Postar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>