<?php
include("./phpDB/post_db_handler.php");
?>
<!DOCTYPE html>
<html>

<head>
    <link type="text/css" rel="stylesheet" href="css/index.css" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="icon" type="image/png" href="./images/logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="./js/check_post.js"></script>


    <title>Manual do Jovem Viajante</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div id="menu-area">
        <img src="./images/logo.png" alt="logo">
        <div id="login-area">
            <button><a href="login.php">LOGIN</a></button>
        </div>
    </div>
    <div id="content">
        <div id="main">
            <div id="main-center">
                <!-- Carrousel Bootstrap -->
                <div id="carouselExampleIndicators" class="carousel slide carrousel-div" data-ride="carousel">
                    <!-- barrinhas de baixo -->
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <!-- Título no meio do carrousel -->
                    <h1>MANUAL DO VIAJANTE</h1>
                    <!-- itens Carrousel -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Pontes se Atravessam</h5>
                                <p>Para mais aventuras</p>
                            </div>
                            <img class="d-block w-100" style="width: 80%; height: 400px;" src="./images/Bridge.jpg" alt="Primeiro Slide">
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>O Pôr do Sol Determina</h5>
                                <p>A hora de descansar</p>
                            </div>
                            <img class="d-block w-100" style="width: 80%; height: 400px;" src="./images/Street-2.jpg" alt="se Slide">
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>A Montanha nos Mostra</h5>
                                <p>Onde podemos chegar</p>
                            </div>
                            <img class="d-block w-100" style="width: 80%; height: 400px;" src="./images/montain.jpg" alt="se Slide">
                        </div>
                    </div>
                    <!-- Controles -->
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Próximo</span>
                    </a>
                </div>
                <!-- Fim Carrousel Bootstrap -->

                <div id="area-desc">
                    <p>O Manual do Viajante é um Diário pessoal de Viajem onde se compartilha Experiências
                        de viajens realizadas por todo o globo.
                    </p>
                </div>
                

                <!-- Posts -->
                <div id="area-posts">
                    <?php 
                    $posts = getAllPosts();
                    if (mysqli_num_rows($posts) > 0): 
                    while($post = mysqli_fetch_assoc($posts)): ?>

                        <div class="post">
                            <div class="post-header">
                                <h2><?= $post['titulo'] ?></h2>
                            </div>
                            <p>10/01/2020</p>
                            <div class="post-img">
                                <img src="<?= $post['img_path'] ?>" alt="imagem">
                            </div>
                            <div class="post-txt">
                                <p><?= $post['texto'] ?></p>
                            </div>
                        </div>
                    <?php endWhile; ?>
                    <?php else: ?>
                        <h3>Nenhum Post Encontrado</h3>
                    <?php endIF; ?>
                </div>               
                <!--Fim dos Posts -->
            </div>
        </div>
        <!--Fim dos Posts -->

    </div>
    <footer>
        <div id="footer_center">
            <p>Site Desenvolvido com muita paciência e tutoriais da web</p>
        </div>
    </footer>


    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>