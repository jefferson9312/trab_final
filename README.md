# Projeto Final DS122 - Desenvolvimento Web

* 1° Aluno: Luiz Felipe Tozati
* 2° Aluno: Renan Kenji Koga Kuroda
* 3° Aluno: Jefferson Santana Filho

## Manual do Viajante

O Projeto consiste num Diário Pessoal, no formato de blog, onde o usuário compartilhará suas expêriencias de viajens pelo mundo  atráves de postagens. **Um post possui Título, Texto, Imagem e Data**. 


##  Sobre o Projeto

O projeto foi desenvolvido num espaço de tempo aproximado de 1.5 semanas, utilizando HTML5, JS e CSS3 e o Framework Bootstrap 4.1.3 no front end, e APACHE, PHP e o banco de dados MYSQL no backend. Também há um sistema de Login, que só serve para Autenticar o único usuário que é o dono do Diário. O usuário é definido logo no setup do banco de dados:
* Email: teste@teste.com
* Senha: 1234 

## Organização das Pastas/Arquivos

Na organização das pastas e arquivos, foi levada em consideração a legibilidade e facilidade de manipulação de código.

* Arquivos contendo o **HTML** das páginas que seriam acessadas, ficaram no diretório Raiz do projeto.
* Arquivos **JS e CSS** ficaram em suas respectivas pastas "./js" e "./css".

* Os arquivos PHP, de propósito geral, que não eram utilizados diretamente no upload/remoção de imagens ou manipulação do banco de dados ficaram na pasta "./lib".
* Arquivos **PHP** que são utilizados diretamente no **upload/remoção de imagens ou manipulação do banco de dados** ficaram na pasta "./phpDB", para facilitar a manutenção desse tipo de código.
* As **imagens** que são inseridas nos posts (sofrem upload) **ficam na pasta "./img_upadas"**.

## Descrições Resumidas dos arquivos

### HTML (No Geral)
Estruturam o Site.

### CSS (No Geral)
Estilizam a estruturação feita no HTML. 

### JS  (No Geral)
Nesse projeto, servem somente para a verificação de campos no Front End, em navegadores com versões menos atualizadas que não fazem essa verificação nativamente.

### index.php
Página principal, onde são carregados os posts, dá acesso à página de login.

### login.php
Página onde se faz a autenticação do único usuário, dá acesso à página de administrador.

### adm_page.php
Página onde se é feito as postagens ou edições/remoções das mesmas, com o botão de "logout" retorna a "index.php".

### edit_post.php
Onde a edição de um Post é feita, ao clicar para submeter retorna à "adm_page.php".

### Pasta ./lib
#### Sanitize.php
"Limpa" a string, cortando espaços, removendo "/" e eliminando HTML injection.
#### Check_post.php
Faz a validação dos campos do post.

### Pasta ./phpDB
#### db_credentials.php
Arquivo onde ficam armazenados os dados necessários para conexão com o banco de dados

#### setup_db_table_test.php
Arquivo, que se executado, cria e usa o banco de dados, além de criar as tabelas  "users" e "post" e criar o único usuário do BD.

#### login_authenticate.php
Responsável pela criação/recuperação de sessões de login.

#### logout.php
Responsável pela destruição de sessões de login.

#### login_db_handler
Responsável por consultar a existência de um usuário no BD.

#### post_db_handler
##### Função "getPost"
* Recupera um post pelo seu id.
##### Função "getAllPosts"
* Recupera todos os Posts.
##### Função "insertPost"
* Cria um novo Post.
##### Função "deletePost"
* Deleta um post e sua imagem pelo seu id.
##### Função "updatePost"
* Faz update de um post pelo seu id.
##### Função "uploadImg"
* Faz upload de uma imagem para a pasta img_uploads, colocando um nome único nessa imagem. Retorna o caminho da imagem no servidor para ser gravado no BD.
##### Função "deleteImg"
* Recupera o caminho da imagem pelo id do post e deleta ela do servidor. É chamada pela função "deletePost".

## Estruturação do Banco de dados
O banco de dados possui duas tabelas e estão estruturadas da seguinte forma.

| users         |	
| ------------- |
| idUser INT(PK)|
| email VARCHAR |
| senha VARCHAR |
 

| posts           |
| --------------- |
| id      INT(PK) |
| idUser  INT(FK) |
| titulo VARCHAR  |
| texto VARCHAR   |
| data_post DATE  |

## Setup do Site
* Checar o db_credentials.
* Executar o arquivo dentro da pasta phpDB "setup_db_table_test.php".
* usar "use blog;" no terminal do MYSQL para usar o BD(Caso necessário).

## Para Logar...
* Email: teste@teste.com
* Senha: 1234 


### Imagens Utilizadas
* Todas as imagens de paisagens utilizadas no projeto foram do site [Unsplash](https://unsplash.com/).
* A logo do site foi desenvolvida pela equipe.