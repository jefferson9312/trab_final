<?php
require("./lib/check_post.php");
include("./phpDB/post_db_handler.php");
require './phpDB/login_authenticate.php';

//Se não está logado vai pra index
if (!isset($_SESSION["user_id"])) {
    header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php");
    exit(); //Termina Execução do Script
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($erro == false && isset($_POST["titulo"]) && isset($_POST["texto"])) {
        insertPost($_POST["titulo"], $_POST["texto"]);

    }
    if (isset($_POST["novotitulo"]) && isset($_POST["novotexto"])) {
        updatePost($_POST["id"], $_POST["novotitulo"], $_POST["novotexto"]);
        $erro_texto = "";
        $erro_titulo = "";
        $erro_img = "";
    }
    if (!$erro) {
        /* limpa o formulário.*/
        $titulo = "";
        $texto = "";
        $erro = false;
    }
}else if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET["acao"]) && isset($_GET["id"])) {
        if ($_GET["acao"] == "remover") {
            deletePost($_GET["id"]);
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <link type="text/css" rel="stylesheet" href="css/index.css" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="icon" type="image/png" href="./images/logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="./js/check_post.js"></script>


    <title>Manual do Jovem Viajante</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div id="menu-area">
        <img src="./images/logo.png" alt="logo">
        <div id="logout-area">
            <button><a href="./phpDB/logout.php">LOGOUT</a></button>
        </div>
    </div>
    <div id="content">
        <div id="main">
            <div id="main-center">

                <!-- Area de criação de Postagem -->
                <div id="centro-area-postagem">
                    <div id="area-postagem">
                        <h1>Nova Postagem</h1>
                        <!--inputs-->
                        <form enctype="multipart/form-data" id="form-test" class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

                            <!-- Titulo -->
                            <div class="form-group <?php if (!empty($erro_titulo)) { echo "has-error";} ?>">
                                <div>
                                    <input required type="text" maxlength="100" class="form-control" autocomplete="off" name="titulo" placeholder="Título" value="<?php echo $titulo; ?>">
                                    <div id="erro-titulo">
                                    </div>
                                    <?php if (!empty($erro_titulo)) : ?>
                                        <span class="help-block"><?php echo $erro_titulo ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <!-- Area de Texto-->
                            <div class="form-group <?php if (!empty($erro_texto)) {
                                                        echo "has-error";
                                                    } ?>">
                                <textarea name="texto" form="form-test" placeholder="Texto da Postagem..." maxlength="1500" required><?php echo $texto; ?></textarea>
                                <div id="erro-texto">
                                </div>
                                <?php if (!empty($erro_texto)) : ?>
                                    <span class="help-block"><?php echo $erro_texto ?></span>
                                <?php endif; ?>
                            </div>

                            <!-- IMG -->
                            <div class="form-group <?php if (!empty($erro_img)) { echo "has-error";} ?>">
                                <label for="inputImg">Imagem</label>
                                <div>
                                    <input required type="file" class="form-control" name="image">
                                    <div id="erro-img">
                                    </div>
                                    <?php if (!empty($erro_img)) : ?>
                                        <span class="help-block"><?php echo $erro_img ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- Botão de Submissão -->
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-success">Postar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Fim Area de criação de Postagem -->

                <!-- Posts -->
                <div id="area-posts">
                    <?php 
                    $posts = getAllPosts();
                    if (mysqli_num_rows($posts) > 0): 
                    while($post = mysqli_fetch_assoc($posts)): ?>

                        <div class="post">
                            <div class="post-header">
                                <h2><?= $post['titulo'] ?></h2>
                                <a href="edit_post.php?id=<?= $post["id"]; ?>">
                                    <button type="button" class="btn btn-primary btn-sm" aria-label="Editar">
                                        <span class="glyphicon glyphicon-pencil">&#x270f;</span>
                                    </button>
                                </a>
                                <a href="<?= $_SERVER["PHP_SELF"] . "?id=" . $post["id"] . "&" . "acao=remover" ?>">
                                    <button type="button" class="btn btn-danger btn-sm" aria-label="Remover">
                                        <span class="glyphicon glyphicon-remove">&times;</span>
                                    </button>
                                </a>
                            </div>
                            <p>10/01/2020</p>
                            <div class="post-img">
                                <img src="<?= $post['img_path'] ?>" alt="imagem">
                            </div>
                            <div class="post-txt">
                                <p><?= $post['texto'] ?></p>
                            </div>
                        </div>
                    <?php endWhile; ?>
                    <?php else: ?>
                        <h3>Nenhum Post Encontrado</h3>
                    <?php endIF; ?>
                </div>               
                <!--Fim dos Posts -->
            </div>
        </div>
        <!--Fim dos Posts -->

    </div>
    <footer>
        <div id="footer_center">
            <p>Site Desenvolvido com muita paciência e tutoriais da web</p>
        </div>
    </footer>


    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>